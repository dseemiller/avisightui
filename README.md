# Setup on Mac (once you have rvm setup ) 

	*Mongo setup via docker 
		mkdir ~/mongo_data 
		docker run -p 27017:27017 --name avi-mongo -v ~/mongo_data/:/data/db -d mongo
		
	*Running rails server (inside the repository) 
		brew install libexif
		bundle install 
		rails server 
		
	*Setting up python environment (inside the repository)
		brew install python3
		brew tap homebrew/science
		brew install opencv3 --with-python3 --with-tbb --without-python --with-contrib --HEAD
		pip3 install -r lib/tasks/requirements.txt

# Setup on Ubuntu

	*Setting up python environment (inside the repository)
		sudo apt-get install -y python3-pip
		sudo apt-get install libopencv-dev python-opencv
		pip3 install -r lib/tasks/requirements.txt