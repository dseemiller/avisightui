class PhotosController < ApplicationController
  before_action :authenticate_user!
  before_action :set_photo, only: [:show, :edit, :update, :destroy, :update_feature]

  def index
    @photos = Photo.all.order_by('metadata.date_time': :desc)
    @locations = Location.all.order_by(poi: :asc)
  end

  def show
  end

  def new
    @photo = Photo.new
    @photo.build_flight_summary
  end

  def edit
  end

  def create
    photos_params[:photos].each do |p|
      @photo = Photo.new(attachment: p[:attachment], flight_summary: FlightSummary.new(flight_params[:flight_summary_attributes]), temperature: photo_params[:temperature])
      @photo.save
    end
    respond_to do |format|
      format.html { redirect_to locations_url, notice: 'Photos successfully uploaded.' }
      format.json { render :show, status: :created, location: @location }
    end
  end

  def update
    respond_to do |format|
      if @photo.update(photo_params)
        flash.now[:success] = 'Photo was successfully updated.'
        format.html { redirect_to @photo, notice: 'Photo was successfully updated.' }
        format.json { render :show, status: :ok, photo: @photo }
        format.js
      else
        flash.now[:error] = 'Unable to update photo.'
        format.html { render :edit }
        format.json { render json: @photo.errors, status: :unprocessable_entity }
        format.js
      end
    end
  end

  def update_feature
    f_idx = @photo.get_feature_index(feature_params[:type], feature_params[:count].to_i)
    @photo.features[f_idx]["user_assessment"] = feature_params[:user_assessment]
    if @photo.save
      flash.now[:success] = 'Photo was successfully updated.'
    else
      flash.now[:error] = 'Unable to update photo.'
    end
    render template: 'photos/update.js'
  end

  def destroy
    @photo.destroy
    respond_to do |format|
      format.html { redirect_to photos_url, notice: 'Photo was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_photo
      @photo = Photo.find(params[:id])
    end

    def photo_params
      params.require(:photo).permit(:location_id, :lat, :lng, :alt, :temperature, :notes, metadata: [:date_time, :make, :model, :exposure_time, :iso_speed_ratings, :exposure_bias_value, :focal_length, :max_aperture_value, :metering_mode], flight_summary: [:operator_name, :aircraft_type, :tail_number, :payload, :notes])
    end

    def photos_params
      params.require :photos
      params.permit photos: [:attachment]
    end
    
    def flight_params
      params.require(:photo).permit(:flight_summary_attributes)
    end

    def feature_params
      params.require(:feature).permit(:count, :type, :user_assessment)
    end
end
