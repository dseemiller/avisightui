class LocationsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_location, only: [:show, :edit, :update, :destroy]

  def index
    @locations = Location.includes(:photos).all
  end

  def show
  end

  def new
    @location = Location.new
  end

  def edit
  end

  def report
    @locations = Location.find(params[:selected_markers].split(',')) 
    respond_to do |format|
      format.pdf do 
        render :pdf => "generated_report",
               :layout => 'pdf.html.erb'
      end
    end
  end

  def create
    @location = Location.new(location_params)

    respond_to do |format|
      if @location.save
        format.html { redirect_to @location, notice: 'Location was successfully created.' }
        format.json { render :show, status: :created, location: @location }
      else
        format.html { render :new }
        format.json { render json: @location.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @location.update(location_params)
        flash.now[:success] = 'Location was successfully updated.'
        format.html { redirect_to locations_url, notice: 'Location was successfully updated.' }
        format.json { render :show, status: :ok, location: @location }
        format.js
      else
        flash.now[:error] = 'Unable to update location.'
        format.html { render :edit }
        format.json { render json: @location.errors, status: :unprocessable_entity }
        format.js
      end
    end
  end

  def destroy
    @location.destroy
    respond_to do |format|
      format.html { redirect_to locations_url, notice: 'Location was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_location
      @location = Location.find(params[:id])
    end

    def location_params
      params.fetch(:location, {}).permit(:lat, :lng, :alt, :temperature, :line_type, :last_inspection, :pole_number, :line_number, :line_type, :last_repair, :last_failure, :last_failure_type, :user_anomaly_detected, photos_attributes: [:attachment])
    end
end
