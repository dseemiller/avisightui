class Location < GeoLocation

  field :poi, type: Integer

  field :line_type, type: Integer
  field :last_inspection, type: Date
  field :pole_number, type: String
  field :line_number, type: String
  field :last_repair, type: Date
  field :last_failure, type: Date
  field :last_failure_type, type: String
  field :user_anomaly_detected, type: Boolean

  has_many :photos, inverse_of: :location

  def name
    "POI #{self.poi}"
  end

end
