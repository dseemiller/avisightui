class Photo < GeoLocation
  include Mongoid::Paperclip

  belongs_to :location, inverse_of: :photos, optional: true
  embeds_one :flight_summary, inverse_of: :photo

  accepts_nested_attributes_for :flight_summary

  field :metadata, type: Hash
  field :notes, type: String
  field :features, type: Array
  
  has_mongoid_attached_file :attachment, styles: { large: "600x600>", medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :attachment, content_type: /\Aimage\/.*\z/

  before_post_process :extract_metadata
  before_create :set_latlng, :set_geolocation, :assign_location
  after_create :find_features

  def extract_metadata
    tempfile = attachment.queued_for_write[:original].path
    data = Exif::Data.new(tempfile) rescue []
    false if data[:gps][:gps_latitude].blank? || data[:gps][:gps_longitude].blank?
    self.metadata = data.to_h
  end

  def set_latlng
    gps = self.metadata[:gps]
    self.lat = gps[:gps_latitude_ref] == 'N' ? gps[:gps_latitude] : -gps[:gps_latitude]
    self.lng = gps[:gps_longitude_ref] == 'E' ? gps[:gps_longitude] : -gps[:gps_longitude]
    self.alt = gps[:gps_altitude]
  end

  def assign_location
    location = nil
    #NOTE: This is a time consuming operation as the no of location's increase we should either improve the query 
    #using mongodb geo capabilities or move it to bacground job 
    Location.all.each do |l|
      if self.distance_to(l) < 10
        location = l
        break
      end
    end
    if location.blank?
      poi = Location.max(:poi) || 0
      location = Location.create(poi: poi + 1, lat: self.lat, lng: self.lng, alt: self.alt)
    end
    self.location = location
  end

  def get_feature_index(type, count)
    self.features.each_with_index do |feature, idx|
      return idx if feature["type"] == type && feature["count"] == count
    end
    nil
  end

  def find_features
    FindFeaturesJob.perform_later self.id.to_s
  end

  def self.confidence_percent(confidence)
    (confidence * 100 / 5).to_i
  end
end
