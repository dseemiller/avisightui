class FindFeaturesJob < ApplicationJob
  queue_as :default

  @@feature_classifiers = ['cld','kingpin','transformer']

  def perform(photo_id)
    photo = Photo.find(photo_id)
    photo_path = photo.attachment.path(:original)
    script = Rails.root.join('lib', 'tasks', 'classifier.py')
    features = []
    @@feature_classifiers.each do |feature_classifier|
      classifier = Rails.root.join('lib', 'tasks', 'features', "#{feature_classifier}.xml")
      feature_location = `python3 #{script} #{classifier} #{photo_path} #{feature_classifier}`
      new_feature = JSON.parse(feature_location)
      features += new_feature
    end
    photo.update(features: features)
  end
end
