json.id location.id.to_s
json.name location.name
json.lat location.lat
json.lng location.lng
json.alt location.alt
json.temperature location.temperature
json.line_type location.line_type
json.last_inspection location.last_inspection
json.pole_number location.pole_number
json.line_number location.line_number
json.last_repair location.last_repair
json.last_failure location.last_failure
json.last_failure_type location.last_failure_type
json.photos location.photos do |photo|
  json.id photo.id.to_s
  json.filename photo.attachment.original_filename
  json.thumb_url photo.attachment.url(:thumb)
  json.medium_url photo.attachment.url(:medium)
  json.thumb_url photo.attachment.url(:large)
  json.original_url photo.attachment.url(:original)
  json.features photo.features do |feature|
    json.name feature["name"]
    json.type feature["type"]
    json.count feature["count"]
    json.confidence_score feature["confidence_score"]
    json.confidence_percent feature["confidence_percent"]
    json.coordinates feature["coordinates"]
    json.color feature["color"]
    json.user_assessment feature["user_assessment"]
  end
  json.notes photo.notes
end