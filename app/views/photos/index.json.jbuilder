json.request do |json|
  json.start params[:start]
  json.limit params[:limit]
end

json.results do |json|
  json.array! @photos, partial: 'photos/photo', as: :photo
end

json.locations do |json|
  json.array! @locations, partial: 'locations/location', as: :location
end