json.id photo.id.to_s
json.url photo.attachment.url(:original)
json.medium_url photo.attachment.url(:medium)
json.lat photo.lat
json.lng photo.lng
json.alt photo.alt
json.temperature photo.temperature
json.date_time photo.metadata[:date_time]
json.make photo.metadata[:make]
json.model photo.metadata[:model]
json.exposure_time photo.metadata[:exposure_time]
json.iso_speed_ratings photo.metadata[:iso_speed_ratings]
json.exposure_bias_value photo.metadata[:exposure_bias_value]
json.focal_length photo.metadata[:focal_length]
json.max_aperture_value photo.metadata[:max_aperture_value]
json.metering_mode photo.metadata[:metering_mode]
json.operator_name photo.flight_summary.operator_name
json.aircraft_type photo.flight_summary.aircraft_type
json.tail_number photo.flight_summary.tail_number
json.payload photo.flight_summary.payload
json.flight_notes photo.flight_summary.notes
json.notes photo.notes
json.location_id photo.location.id.to_s
json.location_name photo.location.name
json.features photo.features do |feature|
  json.name feature["name"]
  json.type feature["type"]
  json.count feature["count"]
  json.confidence_score feature["confidence_score"]
  json.confidence_percent feature["confidence_percent"]
  json.coordinates feature["coordinates"]
  json.color feature["color"]
  json.user_assessment feature["user_assessment"]
end