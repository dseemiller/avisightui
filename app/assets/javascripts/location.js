if(typeof location_id !== "undefined" && location_id !== null) {

  var loc;

  $.getJSON('/locations/' + location_id, function(location) {
    loc = location;
    for(idx in location.photos) {
      photo = location.photos[idx];
      id = photo.id;
      canvas_id = "canvas_" + id;
      for(idx in photo.features) {
        feature = photo.features[idx];
        draw_feature(canvas_id, feature.coordinates, feature.color, 1, feature.count);
      }
    }
    update_feature_list();
  });

  var $carousel = $('#detailsCarousel');

  $carousel.bind('slide.bs.carousel', function (e) {
    if( $.isFunction( $(".carousel-item.active > .panzoom").panzoom ) ) {
      $(".carousel-item.active > .panzoom").panzoom("destroy");
    }
  });

  $carousel.bind('slid.bs.carousel', function (e) {
    update_feature_list();
  });

function update_feature_list() {
  id = $(".carousel-item.active img").attr("id");
  $("#feature-list").empty();
  for(idx in loc.photos) {
    photo = loc.photos[idx];
    if(photo.id == id) {
      for(fidx in photo.features) {
        radio1_checked = "";
        radio2_checked = "";
        feature = photo.features[fidx];
        feature_id = feature.type + "_" + feature.count;
        if(feature.user_assessment == 1) {
          radio1_checked = 'checked="checked"';
        }
        if(feature.user_assessment == 2) {
          radio2_checked = 'checked="checked"';
        }
        $("#feature-list").append('<li class="list-group-item"><div class="col-5">' + feature.name + '</div><div class="col-3"><span class="pull-right">Confidence: ' + feature.confidence_percent + '%</span></div><div class="col-4"><span class="pull-right"><input type="radio" name="' + feature_id + '" value="1" ' + radio1_checked + '> Correct <input type="radio" name="' + feature_id + '" value="2" ' + radio2_checked + '> Incorrect</span></div></li>');
      }
      if(photo.notes == null) photo.notes = '';
      $("#photo_id").val(photo.id);
      $("#photo_notes").val(photo.notes);
      break;
    }
  }
  $(".carousel-item.active > .panzoom").panzoom({$zoomIn: $(".zoom-in"), $zoomOut: $(".zoom-out"), contain: 'invert', panOnlyWhenZoomed: true, increment: .1, minScale: 1, maxScale: 6.6, startTransform: 'scale(1.0)'});
}

$('#user-anomaly').click(function() {
  var checked; 
  if ($(this).is(':checked')) {
    checked = 1;
  } else {
    checked = 0;
  }
  $.ajax({
      type: "PUT",
      url: "/locations/"+$(this).data('location-id')+".js",
      data: { location: {user_anomaly_detected: checked } }
   });
});

$("#photo_form").submit(function() {
  photo_id = $("#photo_id").val();
  notes = $("#photo_notes").val();
  $.ajax({
    type: "PUT",
    url: "/photos/" + photo_id,
    data: { photo: { notes: notes }, _method: 'put', authenticity_token: window._token, format: 'js' }
  });
  return false;
});

$("#photo_form").on("change", "input[type=radio]", function() {
  photo_id = $("#photo_id").val();
  feature = $(this).attr('name').split("_");
  feature_type = feature[0];
  feature_count = feature[1];
  feature_assessment = $(this).val();
  $.ajax({
    type: "PUT",
    url: "/photos/" + photo_id + "/update_feature",
    data: { feature: { type: feature_type, count: feature_count, user_assessment: feature_assessment }, _method: 'put', authenticity_token: window._token, format: 'js' }
  });
  return false;
});

}

function get_scale(original, resized) {
  return scaled_width / original_width;
}

function scale_coords(coords, scale) {
  return [coords[0] * scale, coords[1] * scale, coords[2] * scale, coords[3] * scale];
}

function draw_feature(element, coords, color, scale, title) {
  canvas = document.getElementById(element);
  context = canvas.getContext("2d");
  coords = scale_coords(coords, scale);
  context.strokeStyle = color;
  context.fillStyle = color;
  context.strokeRect(coords[0], coords[1], coords[2], coords[3]);
  if(title != "") context.fillText(title, coords[0] + 2, coords[1] + 10);
}

if(typeof first_location_id !== "undefined" && first_location_id !== null) {
  update_location(first_location_id);
}