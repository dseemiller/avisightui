if ( $( "#myGrid" ).length ) {

var grid;
var loader = new Slick.Data.RemoteModel();
var columns = [
  {id: "id", name: "ID", field: "id", minWidth: 220, width: 220},
  {id: "location", name: "Location", field: "location_id", minWidth: 100, formatter: LocationFormatter, editor: LocationEditor, dataSource: locationsList },
  {id: "url", name: "URL", field: "url", minWidth: 300},
  {id: "date_time", name: "Time", field: "date_time", editor: Slick.Editors.Text, minWidth: 220, width: 220},
  {id: "lat", name: "Lat", field: "lat", editor: Slick.Editors.Float},
  {id: "lng", name: "Long", field: "lng", editor: Slick.Editors.Float},
  {id: "alt", name: "Alt", field: "alt", editor: Slick.Editors.Float},
  {id: "temperature", name: "Temperature", field: "temperature", editor: Slick.Editors.Float},
  {id: "make", name: "Make", field: "make", editor: Slick.Editors.Text},
  {id: "model", name: "Model", field: "model", editor: Slick.Editors.Text},
  {id: "exposure_time", name: "Exposure Time", field: "exposure_time", editor: Slick.Editors.Text, minWidth: 116, width: 116},
  {id: "iso_speed", name: "ISO Speed", field: "iso_speed", editor: Slick.Editors.Text, minWidth: 88, width: 88},
  {id: "exposure_bias", name: "Exposure Bias", field: "exposure_bias", editor: Slick.Editors.Text, minWidth: 116, width: 116},
  {id: "focal_length", name: "Focal Length", field: "focal_length", editor: Slick.Editors.Text, minWidth: 110, width: 110},
  {id: "max_aperture", name: "Max Aperture", field: "max_aperture", editor: Slick.Editors.Text, minWidth: 116, width: 116},
  {id: "metering_mode", name: "Metering Mode", field: "metering_mode", editor: Slick.Editors.Text, minWidth: 116, width: 116},
  {id: "operator_name", name: "Operator Name", field: "operator_name", editor: Slick.Editors.Text, minWidth: 130, width: 130},
  {id: "aircraft_type", name: "Aircraft Type", field: "aircraft_type", editor: Slick.Editors.Text, minWidth: 116, width: 116},
  {id: "tail_number", name: "Tail Number", field: "tail_number", editor: Slick.Editors.Text, minWidth: 116, width: 116},
  {id: "payload", name: "Payload", field: "payload", editor: Slick.Editors.Text},
  {id: "flight_notes", name: "Flight Notes", field: "flight_notes", editor: Slick.Editors.LongText, minWidth: 300, width: 300},
  {id: "notes", name: "Notes", field: "notes", editor: Slick.Editors.LongText, minWidth: 300, width: 300}
];
var options = {
    rowHeight: 24,
    editable: true,
    autoEdit: true,
    enableAddRow: false,
    enableCellNavigation: true,
    enableColumnReorder: false,
    asyncEditorLoading: false
};

$(function () {
  grid = new Slick.Grid("#myGrid", loader.data, columns, options);
  grid.setSelectionModel(new Slick.CellSelectionModel());

  loader.onDataLoaded.subscribe(function (e, args) {
    grid.updateRowCount();
    grid.render();
  });

  grid.onCellChange.subscribe(function (e, args) {
    photo = args.item
    $.post('/photos/' + photo.id, { photo: { location_id: photo.location_id, lat: photo.lat, lng: photo.lng, alt: photo.alt, temperature: photo.temperature, notes: photo.notes, metadata: { make: photo.make, model: photo.model, date_time: photo.date_time, exposure_time: photo.exposure_time, exposure_bias_value: photo.exposure_bias_value, iso_speed_ratings: photo.iso_speed_ratings, focal_length: photo.focal_length, max_aperture_value: photo.max_aperture_value, metering_mode: photo.metering_mode }, flight_summary: { operator_name: photo.operator_name, aircraft_type: photo.aircraft_type, tail_number: photo.tail_number, payload: photo.payload, notes: photo.flight_notes } }, _method:'put', authenticity_token: window._token, format: 'js' });
  });

  grid.onClick.subscribe(function(e, args) {
    if(args.cell == 2) {
      item = args.grid.getData()[args.row];
      $(e.target).popover({html: true, content: '<img src="' + item.medium_url + '">'});
      $(e.target).popover('show');
    }
  });

  $(document).on('click', function (e) {
    $('[data-toggle="popover"],[data-original-title]').each(function () {
      if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {                
        (($(this).popover('hide').data('bs.popover') || {}).inState || {}).click = false
      }
    });
  });

  loader.ensureData(0, 50);
});

function populateSelect(select, dataSource, addBlank) {
  var index, len, newOption;
  if (addBlank) { select.appendChild(new Option('', '')); }
  $.each(dataSource, function (value, text) {
    newOption = new Option(text, value);
    select.appendChild(newOption);
  });
};

function LocationEditor(args) {
  var $input;
  var defaultValue;
  var scope = this;
  var calendarOpen = false;
  
  this.keyCaptureList = [ Slick.keyCode.UP, Slick.keyCode.DOWN, Slick.keyCode.ENTER ];
  
  this.init = function () {
    $input = $('<select></select>');
    $input.width(args.container.clientWidth + 3);
    populateSelect($input[0], args.column.dataSource, true);
    $input.appendTo(args.container);
    $input.focus().select();
    $input.select2({
      placeholder: '-',
      allowClear: true
    });
  };
    
  this.destroy = function () {
    $input.select2('destroy');
    $input.remove();
  };

  this.show = function () {};

  this.hide = function () {
    $input.select2('results_hide');
  };
    
  this.position = function (position) {};

  this.focus = function () {
    $input.select2('input_focus');
  };
  
  this.loadValue = function (item) {
    defaultValue = item[args.column.field];
    $input.val(defaultValue);
    $input[0].defaultValue = defaultValue;
    $input.trigger("change.select2");
  };
    
  this.serializeValue = function () {
    return $input.val();
  };
  
  this.applyValue = function (item, state) {
    item[args.column.field] = state;
  };
  
  this.isValueChanged = function () {
    return (!($input.val() == "" && defaultValue == null)) && ($input.val() != defaultValue);
  };
  
  this.validate = function () {
    return {
      valid: true,
      msg: null
    };
  };

  this.init();
}

function LocationFormatter(row, cell, value, columnDef, dataContext) {
    return columnDef.dataSource[value] || '-';
}

}