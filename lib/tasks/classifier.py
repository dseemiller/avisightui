import sys
import cv2
import numpy as np
import json

FEATURE_MAP = { 'cld': { 'name': 'Ceramic Line Disconnect', 'color': 'red' }, 'kingpin': { 'name': 'Ceramic Pin Insulator', 'color': 'blue' }, 'transformer': { 'name': 'PT Transformer', 'color': 'green' } };
CONFIDENCE_MAX = 5
X_SCALE = 0.15
Y_SCALE = 0.15
ZOOM_FACTOR = 1.2
NEIGHBORS = 4

def main(classifier_path, image_path, feature):
  img = create_image(image_path)
  classifier = cv2.CascadeClassifier(classifier_path)
  features, levels, confidence = classifier.detectMultiScale3(img, ZOOM_FACTOR, NEIGHBORS, outputRejectLevels=True)
  all_features = []
  if(len(features) > 0):
    feature_list = features.tolist()
    feature_count = 1
    for idx, f in enumerate(feature_list):
      conf_percent = confidence_percent(confidence[idx][0])
      if conf_percent > 0:
        all_features.append({'name': feature_name(feature, feature_count), 'type': feature, 'coordinates': f, 'count': feature_count, 'confidence_score': confidence[idx][0], 'confidence_percent': conf_percent, 'color': feature_color(feature)})
        feature_count += 1
  print(json.dumps(all_features))

def feature_name(feature, counter):
  return "{0} #{1}".format(FEATURE_MAP[feature]['name'], counter)

def feature_color(feature):
  return FEATURE_MAP[feature]['color']

def confidence_percent(confidence):
  return int(round(((confidence * 100) / CONFIDENCE_MAX), 0))

def create_image(image_path):
  img = cv2.imread(image_path)
  img = cv2.resize(img, (0,0), fx=X_SCALE, fy=Y_SCALE)
  return cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

if __name__ == "__main__":
  classifier = sys.argv[1]
  img = sys.argv[2]
  feature = sys.argv[3]
  main(classifier, img, feature)
