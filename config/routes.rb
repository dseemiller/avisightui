Rails.application.routes.draw do

  devise_for :users

  put '/photos/:id/update_feature', to: 'photos#update_feature'

  resources :photos, except: [:edit] do 
  	collection do 
  		get :get_data, defaults: { format: 'json' }
  	end
  end

  resources :locations, except: [:new, :create] do 
  	collection do
      post :report
  	end
  end

  root to: "locations#index"

end
