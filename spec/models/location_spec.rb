require 'rails_helper'

RSpec.describe Location, type: :model do
  context "When location is create" do
  	
  	before(:each) { @location = Location.create! }
    
    it "should have name automatically assigned" do
      expect(@location.name).not_to be_empty
    end
    
    it "should have name in a specific format" do
      expect(@location.name).to match(/^poi_/)
    end

	end
end
